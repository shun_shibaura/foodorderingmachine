import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JButton saladButton;
    private JButton soupButton;
    private JButton pizzaButton;
    private JButton tiramisuButton;
    private JButton doriaButton;
    private JButton spaghettiButton;
    private JButton checkOutButton;
    private JTextPane orderedItemsList;
    private JTextPane totalPrice;
    private JLabel whatWouldYouLikeLabel;
    private JLabel salad350yenLabel;
    private JLabel soup150yenLabel;
    private JLabel pizza400yenLabel;
    private JLabel doria320yenLabel;
    private JLabel orderedItemsLabel;
    private JLabel tiramisu330YenLabel;
    private JLabel spaghetti540YenLabel;

    private int total = 0;

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            String currentText = orderedItemsList.getText();
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            orderedItemsList.setText(currentText + food + "   " + price + " yen\n");
            total += price;
            totalPrice.setText("Total     " + total + "yen");

        }
    }

    public FoodOrderingMachine() {
        saladButton.setIcon(new ImageIcon(
                this.getClass().getResource("Salad.jpg")
        ));
        soupButton.setIcon(new ImageIcon(
                this.getClass().getResource("Soup.jpg")
        ));
        pizzaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Pizza.jpg")
        ));
        doriaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Doria.jpg")
        ));
        spaghettiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Spaghetti.jpg")
        ));
        tiramisuButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tiramisu.jpg")
        ));
        totalPrice.setText("Total     " + total + "yen");

        saladButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Salad", 350);
            }
        });
        soupButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soup", 150);
            }
        });
        pizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza", 400);
            }
        });
        doriaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Doria", 320);
            }
        });
        spaghettiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Spaghetti", 540);
            }
        });
        tiramisuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tiramisu", 330);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    String currentText = orderedItemsList.getText();
                    JOptionPane.showMessageDialog(null, "Thank you! the total price is " + total + " yen.");
                    total = 0;
                    orderedItemsList.setText("");
                    totalPrice.setText("Total     " + total + "yen");

                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
